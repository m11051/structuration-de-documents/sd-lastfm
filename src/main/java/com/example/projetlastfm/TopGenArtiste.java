package com.example.projetlastfm;

import org.bson.BsonArray;
import org.bson.BsonDocument;
import org.bson.BsonValue;
import org.bson.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TopGenArtiste {
    Map<String, Integer> artistes = new HashMap<>();

    public TopGenArtiste(Document doc) {
        BsonDocument bsonDoc = (BsonDocument) doc.toBsonDocument().get("artists");
        for (BsonValue value : bsonDoc.getArray("artist")) {
            BsonDocument docValue = value.asDocument();
            this.artistes.put(docValue.get("name").asString().getValue(), Integer.valueOf(docValue.get("playcount").asString().getValue()));
        }
        //String bsonDc = bsonDoc.asString().getValue();
    }

    public String toString(){
        String out="";
        for (Map.Entry<String, Integer> entry : this.artistes.entrySet()) {
            out+=(entry.getKey() + " = " + entry.getValue() + " écoutes\n");
        }
        return out;
    }
}
