package com.example.projetlastfm;

import org.bson.BsonDocument;
import org.bson.BsonValue;
import org.bson.Document;

import java.util.HashMap;
import java.util.Map;

public class TopGeoArtist {
    String lieu;
    Map<String, Integer> artistes = new HashMap<>();

    public TopGeoArtist(String lieu, Document doc) {
        this.lieu=lieu;
        BsonDocument bsonDoc = (BsonDocument) doc.toBsonDocument().get("topartists");
        for (BsonValue value : bsonDoc.getArray("artist")) {
            BsonDocument docValue = value.asDocument();
            this.artistes.put(docValue.get("name").asString().getValue(), Integer.valueOf(docValue.get("listenersS").asString().getValue()));
        }
        //String bsonDc = bsonDoc.asString().getValue();
    }

    public String toString(){
        String out=" ----- " + this.lieu + " ----- \n\n";
        for (Map.Entry<String, Integer> entry : this.artistes.entrySet()) {
            out+=(entry.getKey() + " = " + entry.getValue() + " écoutes\n");
        }
        return out;
    }
}
