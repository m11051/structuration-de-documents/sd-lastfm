package com.example.projetlastfm;

import org.bson.BsonDocument;
import org.bson.Document;

import java.util.ArrayList;

public class Album {
    public String nom;
    public String artiste;
    public String tag;
    public ArrayList<String> morceaux;
    public String urlAlbum;
    public int portee;
    public String resume;
    ArrayList<Avis> avis = new ArrayList<>();


    public Album(Document doc, boolean typeCrea) {
        //Sil s'agit d'une création d'album via une requete API
        if(!typeCrea) {
            BsonDocument bsonDoc = (BsonDocument) doc.toBsonDocument().get("album");
            this.nom = bsonDoc.get("name").asString().getValue();
            this.urlAlbum = bsonDoc.get("url").asString().getValue();
            this.tag="coucou";
            this.artiste=bsonDoc.get("artist").asString().getValue();;
            try{this.portee = Integer.parseInt(bsonDoc.get("playcount").asString().getValue());}
            catch(Exception e){bsonDoc.get("playcount").asInt32().getValue();}
            BsonDocument description = (BsonDocument) bsonDoc.get("wiki");
            this.resume = description.get("summary").asString().getValue();
        }//sinon par une requete BDD
        else{
            BsonDocument bsonDoc = doc.toBsonDocument();
            this.nom = bsonDoc.get("nom").asString().getValue();
            this.urlAlbum = bsonDoc.get("url").asString().getValue();
            this.tag = bsonDoc.get("tag").asString().getValue();
            this.artiste=bsonDoc.get("artiste").asString().getValue();
            this.portee = bsonDoc.get("portee").asInt32().getValue();
            this.resume = bsonDoc.get("resume").asString().getValue();
        }
    }


    public void addAvis(ArrayList<Document> doc){
        for(Document docUnique : doc){
            this.avis.add(new Avis(docUnique, true));
        }
    }


    public String toString(){
        String sortieAvis="";
        for (Avis avis : this.avis) {
            sortieAvis+=avis.nomUser+" : "+avis.commentaire+" ( "+avis.note+"/10)\n";
        }
        return " --- " + this.nom + " --- " +
                "\n\nArtiste : " + this.artiste +
                "\nPortée : " + this.portee +
                "\nUrl LASTFM : " + this.urlAlbum +
                "\n\n" + this.resume+"\n\n--------------Avis--------------\n" +sortieAvis;
    }

}
