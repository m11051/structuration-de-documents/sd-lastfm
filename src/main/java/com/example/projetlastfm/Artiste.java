package com.example.projetlastfm;

import org.bson.BsonDocument;
import org.bson.BsonValue;
import org.bson.Document;

import java.util.ArrayList;

public class Artiste {
    public String nom;
    public int auditeurs;
    public int ecoutes;
    public String urlLastFM;
    public ArrayList<Artiste> similaires = new ArrayList<>();
    public ArrayList<Album> albums = new ArrayList<>();
    public ArrayList<Tag> tags = new ArrayList<>();
    public String resume;
    ArrayList<Avis> avis = new ArrayList<>();

    public Artiste(Document doc, boolean typeCrea) {
        //Sil s'agit d'une création d'artiste via une requete API
        if(!typeCrea) {
            BsonDocument bsonDoc = (BsonDocument) doc.toBsonDocument().get("artist");
            this.nom = bsonDoc.get("name").asString().getValue();
            BsonDocument stats = (BsonDocument) bsonDoc.get("stats");
            this.auditeurs = Integer.parseInt(stats.get("listeners").asString().getValue());
            this.ecoutes = Integer.parseInt(stats.get("playcount").asString().getValue());
            this.urlLastFM = bsonDoc.get("url").asString().getValue();
            this.similaires = null;
            this.tags = null;
            BsonDocument description = (BsonDocument) bsonDoc.get("bio");
            this.resume = description.get("summary").asString().getValue();
            //recupération du fichier à ajouter à la BDD
        }
        //sinon par une requete BDD
        else{
            BsonDocument bsonDoc = doc.toBsonDocument();
            this.nom = bsonDoc.get("nom").asString().getValue();
            this.auditeurs = bsonDoc.get("auditeur").asInt32().getValue();
            this.ecoutes = bsonDoc.get("ecoutes").asInt32().getValue();
            this.urlLastFM= bsonDoc.get("urllastFM").asString().getValue();
            this.similaires = null;
            this.tags = null;
            this.resume = bsonDoc.get("Resume").asString().getValue();
        }
    }

    public void setTag(Document doc){
        AppelsAPI appelsAPI = new AppelsAPI();
        BsonDocument bsonDoc = (BsonDocument) doc.toBsonDocument().get("tag");
        this.tags.add(appelsAPI.getTag(bsonDoc.get("name").asString().getValue()));
    }


    public void addAvis(ArrayList<Document> doc){
        for(Document docUnique : doc){
            this.avis.add(new Avis(docUnique, true));
        }
    }

    public String toString(){
        String sortieAvis="";
        for (Avis avis : this.avis) {
            sortieAvis+=avis.nomUser+" : "+avis.commentaire+" ( "+avis.note+"/10)\n";
        }
        return " --- " + this.nom + " --- " +
               "\n\nAuditeurs : " + this.auditeurs +
               "\nEcoutes : " + this.ecoutes +
                "\nUrl LASTFM : " + this.urlLastFM +
               "\n\n" + this.resume +"\n\n--------------Avis--------------\n" +sortieAvis;
    }

}
