package com.example.projetlastfm;

import org.bson.BsonArray;
import org.bson.BsonDocument;
import org.bson.BsonValue;
import org.bson.Document;

import java.util.HashMap;
import java.util.Map;

public class TopGenTag {
    Map<String, Integer> tags = new HashMap<>();

    public TopGenTag(Document doc) {
        BsonDocument bsonDoc = (BsonDocument) doc.toBsonDocument().get("tags");
        for (BsonValue value : bsonDoc.getArray("tag")) {
            BsonDocument docValue = value.asDocument();
            this.tags.put(docValue.get("name").asString().getValue(), Integer.valueOf(docValue.get("taggings").asString().getValue()));
        }
    }

    public String toString(){
        String out="";
        for (Map.Entry<String, Integer> entry : this.tags.entrySet()) {
            out+=(entry.getKey() + " = " + entry.getValue() + " objets taggés\n");
        }
        return out;
    }
}
